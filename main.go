
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

type ver struct {
	AppID          string `json:"AppID"`
	AppTitle       string `json:"AppTitle"`
	AppDesc        string `json:"AppDesc"`
}

type AllVersion []ver

var version = AllVersion{{
		AppID       : "1.0",
		AppTitle    : "Golang 1",
		AppDesc     : "Saya Coba Belajar OOP Go",
	},}

func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, You can access /version -> Version , /GetAbsensi -> Absensi Data")
  log.Println(r.RequestURI)
}

func AppVersion(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(version)
  log.Println(r.RequestURI)
}

func GetDataAbsensi(w http.ResponseWriter, r *http.Request) {
  resp, err := http.Get("https://attendance.propernas.co.id/api/getData/05/01/2021/1")
    if err != nil {
       log.Fatalln(err)
    }
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
       log.Fatalln(err)
    }
    sb := string(body)
    fmt.Fprintf(w, sb)
    log.Println(r.RequestURI)
}
func main() {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", homeLink)
  router.HandleFunc("/GetAbsensi", GetDataAbsensi).Methods("GET")
  router.HandleFunc("/version", AppVersion).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", router))
}
